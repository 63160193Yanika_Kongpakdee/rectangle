/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yanika.rectangle1;

/**
 *
 * @author WINDOWS10
 */
public class Rectangle {
    double s ;
    public Rectangle (double s){
        this.s = s ;
    }
    public double calArea(){
        return s * s ;
    }
    public double getR(){
        return s ;
    } public void setR(double s){
        if (s <= 0){
            System.out.println("Error: Radius must more than zero!!!");
            return ;
        }
        this.s = s ;
    }
    @Override 
    public String toString() {
       return "Area of rectangle(s = " + this.getR()+ ") is " + this.calArea();
    }
}
